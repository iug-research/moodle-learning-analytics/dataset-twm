# Datasheet for twm-restore

## Table of Contents

1. [Motivation](#motivation)
2. [Composition](#composition)
3. [Collection Process](#collection-process)
4. [Preprocessing/Cleaning/Labeling](#preprocessingcleaninglabeling)
5. [Uses](#uses)
6. [Distribution](#distribution)
7. [Maintenance](#maintenance)

## 1. Motivation <a name="motivation"></a>

1. **For what purpose was the dataset created?** 
twm-restore.mbz was created to be importable into Moodle instances and serve as example training and evaluation data for Learning Analytics models like the dropout prediction model. More specifically, it can be used to try out the Moodle plugin [LaLA](https://github.com/LiFaytheGoblin/moodle-tool_lala) which aims to make Moodle Learning Analytics auditable.

2. **Who created the dataset and on behalf of which entity?**
twm-restore.mbz was reconstructed by Linda Fernsel in the [Informatik und Gesellschaft](https://iug.htw-berlin.de/) research group at the University of Applied Sciences (HTW) in Berlin. It is based on the "Learn Moodle" data set, formerly published by Elizabeth Dalton on behalf of Moodle Pty Ltd on research.moodle.net under a CC-BY 3.0 license (the original readme is included in this repository in the folder "original").

3. **Who funded the creation of the dataset?** 
The creation of this data set was funded by the Federal Ministry of Education and Research of Germany as part of the project Fair Enough? (project nr: 16DHB4002) at the University of Applied Sciences (HTW) Berlin.

4. **Any other comments?**
No.

## 2. Composition <a name="composition"></a>

1. **What do the instances that comprise the dataset represent?**
The dataset has the form of a Moodle backup file (.mbz) which contains XML files on
* The course structure, modules and properties
* Users enrolled in the course including their
  * activity log
  * grades
  * profile information such as timezone

2. **How many instances are there in total?**
* 1 course
* 6 sections
* 60 course modules
* 2169 users, including 2 teachers

3. **Does the dataset contain all possible instances or is it a sample (not necessarily random) of instances from a larger set?** 
The original dataset only contained data from those users who consented to the data collection and use (35% of course participants).

4. **What data does each instance consist of?** 
The mbz-folder consists of further folders and XML-files. The folder structure is as follows:
```bash
── activities
│   ├── assign_43
│   ├── book_10
│   ├── book_21
│   ├── book_33
│   ├── book_9
│   ├── chat_11
│   ├── chat_27
│   ├── chat_38
│   ├── chat_52
│   ├── choice_14
│   ├── choice_24
│   ├── data_35
│   ├── feedback_13
│   ├── feedback_45
│   ├── folder_59
│   ├── folder_60
│   ├── forum_1
│   ├── forum_29
│   ├── forum_34
│   ├── forum_40
│   ├── forum_49
│   ├── forum_55
│   ├── forum_7
│   ├── forum_8
│   ├── glossary_22
│   ├── label_17
│   ├── label_28
│   ├── label_39
│   ├── label_4
│   ├── label_53
│   ├── label_6
│   ├── lesson_46
│   ├── page_15
│   ├── page_16
│   ├── page_18
│   ├── page_19
│   ├── page_2
│   ├── page_26
│   ├── page_3
│   ├── page_30
│   ├── page_36
│   ├── page_37
│   ├── page_41
│   ├── page_44
│   ├── page_5
│   ├── page_50
│   ├── page_51
│   ├── page_54
│   ├── page_56
│   ├── page_57
│   ├── page_58
│   ├── quiz_12
│   ├── quiz_20
│   ├── quiz_32
│   ├── quiz_42
│   ├── quiz_47
│   ├── resource_31
│   ├── survey_23
│   ├── wiki_25
│   └── workshop_48
├── course
├── files
│   └── 02
└── sections
    ├── section_1
    ├── section_2
    ├── section_3
    ├── section_4
    ├── section_5
    └── section_6
```

An activity may have the following XML files, e.g.:

```bash
│   ├── assign_43
│   │   ├── assign.xml
│   │   ├── calendar.xml
│   │   ├── comments.xml
│   │   ├── competencies.xml
│   │   ├── completion.xml
│   │   ├── filters.xml
│   │   ├── grade_history.xml
│   │   ├── grades.xml
│   │   ├── grading.xml
│   │   ├── inforef.xml
│   │   ├── logstores.xml
│   │   ├── logs.xml
│   │   ├── module.xml
│   │   ├── roles.xml
│   │   └── xapistate.xml
```

or

```bash
│   ├── forum_49
│   │   ├── calendar.xml
│   │   ├── comments.xml
│   │   ├── competencies.xml
│   │   ├── completion.xml
│   │   ├── filters.xml
│   │   ├── forum.xml
│   │   ├── grade_history.xml
│   │   ├── grades.xml
│   │   ├── grading.xml
│   │   ├── inforef.xml
│   │   ├── logstores.xml
│   │   ├── logs.xml
│   │   ├── module.xml
│   │   ├── roles.xml
│   │   └── xapistate.xml
```

Activity logs are stored in the "logstores.xml" files in the folder concerned by the respective logs.

5. **Is there a label or target associated with each instance?**
No.

6. **Is any information missing from individual instances?** 
The user data does not contain any personal data such as names, ip-adresses or user-generated content. The original data was further anonymized in this repository by removing all content from the "other" column in the logstore table. The resulting data set did not allow to exactly reconstruct the course structure. There are possibly course modules missing and the order might be wrong. Furthermore, the course modules are "dummy" elements without any real content. 

7. **Are relationships between individual instances made explicit?**
The structure of the folder reveals some relationships - the logs and grades belonging to a course module are stored in the respective folder.

8. **Are there recommended data splits?** 
No.

9. **Are there any errors, sources of noise, or redundancies in the dataset?** 
The reconstruction of the course tried to approximate the original course structure and content but should not be seen as a perfect reconstruction. As written before, a lot of data that would really be exported from a Moodle course is not contained. 

10. **Is the dataset self-contained, or does it link to or otherwise rely on external resources?** 
The data set is in a form that is most useful when imported into a Moodle system. The import has been tested with Moodle 4.2.

11. **Does the dataset contain data that might be considered confidential?** 
No.

12. **Does the dataset contain data that, if viewed directly, might be offensive, insulting, threatening, or might otherwise cause anxiety?** 
No.

13. **Does the dataset identify any subpopulations?**

User data contains the following data that can be used to form subpopulations (the most common values (> 30 users) and their distribution are listed): 
* "lang" (which language did the user use Moodle in?)
  * english ("en", "en_US"): 1740 users
  * spanish ("es", "es_mx", "es_ve"): 118 users
  * german ("de"): 45 users
  * portuguese ("pt", "pt_br"): 42 users
  * italian ("it"): 37 users
  * french ("fr", "fr_ca"): 31 users
* "timezone": 
  * "99" (the default time zone): 1074
  * "America/New_York": 108
  * "Europe/London": 89
  * "Australia/Sydney": 56
  * "America/Chicago": 52
  * "Asia/Kolkata": 45
  * "Australia/Melbourne": 42
  * "Asia/Karachi": 33

14. **Is it possible to identify individuals (i.e., one or more natural persons), either directly or indirectly (i.e., in combination with other data) from the dataset?** 
No.

15. **Does the dataset contain data that might be considered sensitive in any way?** 
The user's language and timezone settings.

16. **Any other comments?**
No.

## 3. Collection Process <a name="collection-process"></a>
See the original readme.

## 4. Preprocessing/Cleaning/Labeling <a name="preprocessingcleaninglabeling"></a>

1. **Was any preprocessing/cleaning/labeling of the data done?** 

See the original readme. Furthermore:
* The logstore was truly anonymized by removing data from the "other" column.
* The columns "icq", "phone1", "phone2", "institution", "department", "address", "city", "country", "url", "description" and "theme" from the user data were dropped.
* The information "email", "firstname", "lastname" were added to the user data (contructed from the random number in the username)
* Two users were identified as teachers based on their activity data and thus enrolled in the course as teachers instead of students.
* The grade history was reduced to only the most recent grade per student per activity and transformed into a CSV file that can be imported to the Moodle "gradebook". It was used to reconstruct the grades in the mbz file.
* Through the transformation process, new ids were assigned to entities.

2. **Was the "raw" data saved in addition to the preprocessed/cleaned/labeled data?**
Yes, see the folder "original".

3. **Is the software that was used to preprocess/clean/label the data available?** 
Yes, the data transformation [CSV to MBZ pipeline](https://gitlab.com/iug-research/moodle-learning-analytics/csv-to-mbz-pipeline/-/tree/twm?ref_type=heads) is available (branch `twm`).

4. **Any other comments?**
No.

## 5. Uses <a name="uses"></a>

1. **Has the dataset been used for any tasks already?**
The dataset serves as a test data set for the LaLA plugin.

2. **Is there a repository that links to any or all papers or systems that use the dataset?** 
No.

3. **What (other) tasks could the dataset be used for?**
Further test Moodle Learning Analytics - however, the data set is too small for training a model and would need to be complemented with further mbz files.

4. **Is there anything about the composition of the dataset or the way it was collected and preprocessed/cleaned/labeled that might impact future uses?** 
Different versions of Moodle might not be able to restore the mbz in the same ways.

5. **Are there tasks for which the dataset should not be used?**
No.

6. **Any other comments?**
No.

## 6. Distribution <a name="distribution"></a>

1. **Will the dataset be distributed to third parties outside of the entity on behalf of which the dataset was created?** 
Yes, the dataset is published via this repository.

2. **How will the dataset be distributed?**
GitLab.

3. **When will the dataset be distributed?**
December 2023

4. **Will the dataset be distributed under a copyright or other intellectual property (IP) license, and/or under applicable terms of use (ToU)?** 
[CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

5. **Have any third parties imposed IP-based or other restrictions on the data associated with the instances?**
No.

6. **Do any export controls or other regulatory restrictions apply to the dataset or to individual instances?**
No.

7. **Any other comments?**
No.

## 7. Maintenance <a name="maintenance"></a>

1. **Who will be supporting/hosting/maintaining the dataset?**
The [Informatik und Gesellschaft](https://iug.htw-berlin.de/) research group at HTW Berlin.

2. **How can the owner/curator/manager of the dataset be contacted?**
See [the website](https://iug.htw-berlin.de/)

3. **Is there an erratum?**
No.

4. **Will the dataset be updated?** 
No.

5. **If the dataset relates to people, are there applicable limits on the retention of the data associated with the instances?** 
People consented to the use of their data for research purposes only.

6. **Will older versions of the dataset continue to be supported/hosted/maintained?** 
Older versions (if any) will be available via Git versioning.

7. **If others want to extend/augment/build on/contribute to the dataset, is there a mechanism for them to do so?**
No.

8. **Any other comments?**
No.
   
# About Datasheets for Datasets
Datasheets for Datasets is a documentation method for data that was introduced in the following publication:

Timnit Gebru, Jamie Morgenstern, Briana Vecchione, Jennifer Wortman Vaughan, Hanna Wallach, Hal Daumé III, and Kate Crawford. 2021. Datasheets for datasets. Commun. ACM 64, 12 (December 2021), 86–92. https://doi.org/10.1145/3458723
